<?php


namespace App\Http\Services;


use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface OrderServiceInterface
{
    public function findAll(): Collection;

    public function find(int $id): ?Order;

    public function delete(int $id): ?int;

    public function store(Request $request): Order;

    public function update(Request $request, Order $order): void;
}