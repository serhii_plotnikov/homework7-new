<?php


namespace App\Http\Services;


use App\Order;
use App\OrderItem;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class OrderService implements OrderServiceInterface
{
    public function findAll(): Collection
    {
        return Order::all();
    }

    public function find(int $id): ?Order
    {
        return Order::find($id);
    }

    public function delete(int $id): ?int
    {
        return Order::destroy($id);
    }

    public function store(Request $request): Order
    {
        if (!$request->has('date')) {
            $date = Carbon::now()->addDays(3);
        }
        $order = Order::create(['buyer_id' => $request->buyerId, 'date' => $date]);
        $orderItems = [];
        foreach ($request->orderItems as $orderItem) {
            $product = Product::find($orderItem['productId']);
            $params = $this->transformParams($orderItem);
            $params['price'] = $product->price;
            $params['sum'] = $this->sum($params['quantity'], $params['price'], $params['discount']);
            $params['product_id'] = $product->id;
            $orderItems[] = new OrderItem($params);
        }
        $order->orderItems()->saveMany($orderItems);
        return $order;
    }

    public function update(Request $request, Order $order):void
    {
        foreach ($request->orderItems as $orderItem) {
            $product = Product::find($orderItem['productId']);
            $params = $this->transformParams($orderItem);
            $params['sum'] = $this->sum($params['quantity'], $product->price, $params['discount']);
            OrderItem::where('product_id', $product->id)->update($params);
        }
    }

    private function transformParams(array $params)
    {
        return [
            'quantity' => $params['productQty'],
            'discount' => (int)$params['productDiscount'] / 100
        ];
    }

    private function sum(int $quantity, int $price, float $discount): int
    {
        return round($quantity * $price * (1 - $discount));
    }
}