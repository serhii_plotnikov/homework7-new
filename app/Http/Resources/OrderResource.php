<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $response = [
            'orderId' => $this->id,
            'orderDate' => $this->date
        ];
        $orderSum = 0;
        foreach ($this->orderItems as $orderItem) {
            $item['productName'] = $orderItem->product->name;
            $item['productQty'] = $orderItem->quantity;
            $item['productPrice'] = $orderItem->price / 100;
            $item['productDiscount'] = $orderItem->discount * 100 . '%';
            $item['productSum'] = $orderItem->sum / 100;
            $response['orderItems'][] = $item;
            $orderSum += $item['productSum'];
        }
        $response['orderSum'] = round($orderSum, 2);
        $response['buyer']['buyerFullName'] = "{$this->buyer->name} {$this->buyer->surname}";
        $response['buyer']['buyerAddress'] = "{$this->buyer->country}, {$this->buyer->city}, {$this->buyer->addressLine}";
        $response['buyer']['phone'] = $this->buyer->phone;

        return $response;
    }
}
