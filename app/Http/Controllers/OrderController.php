<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use App\Http\Services\OrderServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

class OrderController extends Controller
{
    private $orderService;

    public function __construct(OrderServiceInterface $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Collection
     */
    public function index()
    {
        $orders = $this->orderService->findAll();
        return $orders->map(function ($order) {
            return new OrderResource($order);

        });
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return OrderResource
     */
    public function store(Request $request)
    {
        $order = $this->orderService->store($request);
        return new OrderResource($order);


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return OrderResource|Response
     */
    public function show(int $id)
    {
        $order = $this->orderService->find($id);
        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'product not found'
            ]);
        }
        return new OrderResource($order);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response|OrderResource
     */
    public function update(Request $request, int $id)
    {
        $order = $this->orderService->find($id);
        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }
        foreach ($order->orderItems as $orderItem) {
            $productsIds[] = $orderItem->product_id;
        }
        foreach ($request->orderItems as $orderItem) {
            if (!in_array($orderItem['productId'], $productsIds)) {
                return new Response([
                    'result' => 'fail',
                    'message' => 'productItem not found'
                ]);
            }
        }
        $this->orderService->update($request, $order);
        return new OrderResource($this->orderService->find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array
     */
    public function destroy(int $id)
    {
        $result = $this->orderService->delete($id);
        return ['result' => $result ? 'success' : 'fail'];
    }
}
