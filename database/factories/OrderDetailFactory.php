<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'quantity' => $faker->numberBetween(1, 10),
        'price' => $faker->numberBetween(100, 1000),
        'discount' => $faker->randomFloat(2, 0.1, 0.7),
    ];
});
