<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    private $orderItemId;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->getOrderItemId();
        factory(\App\Buyer::class, 10)->create()->each(function (\App\Buyer $buyer) {
            $buyer->orders()->saveMany(
                factory(\App\Order::class, 4)->make(['buyer_id' => null]))->each(function (\App\Order $order) {
                $order->orderItems()->saveMany(
                    factory(\App\OrderItem::class, 5)->make(['order_id' => null,
                        'sum' => 0])->each(function (\App\OrderItem $orderItem) {
                        $orderItem->sum = $this->getSum($orderItem);
                        $orderItem->product()->associate(\App\Product::find($this->orderItemId++));
                    })
                );
            });
        });
    }

    private function getSum(\App\OrderItem $orderItem): float
    {
        return round($orderItem->quantity * $orderItem->price * (1 - $orderItem->discount));
    }

    private function getOrderItemId(): void
    {
        $orderItemId = \App\OrderItem::all()->last();
        $this->orderItemId = !is_null($orderItemId) ? ($orderItemId->getAttributes()['id'] + 1) : 1;
    }
}
